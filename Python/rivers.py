import pywikibot, re

site = pywikibot.Site("lv", "wikipedia")
datasite = pywikibot.Site("wikidata", "wikidata")
repo = datasite.data_repository()

articles = ['Sprungu strauts‎']

importedLvWikipedia = pywikibot.Claim(repo, u'P143')
lvWikipedia = pywikibot.ItemPage(datasite, u'Q728945')
importedLvWikipedia.setTarget(lvWikipedia)

def pagename(bg):
	bg = re.sub('\s*(\([^\(]+)$','',bg)
	
	return bg

def simpleAdd(item,repo,prop,data):

	newClaim = pywikibot.Claim(repo,prop)
	newClaim.setTarget(data)
	
	item.addClaim(newClaim)
	newClaim.addSources([importedLvWikipedia])
	

def main():
	for article in articles:
		page = pywikibot.Page(site,article)
		
		articleTitle = page.title()
		pywikibot.output("Working on %s" % articleTitle)
			
		print('creating item')
		data = {
				'labels': {
					'lv': { 'language': 'lv', 'value': pagename(articleTitle) }
					}
				}
		item = pywikibot.ItemPage(page.site.data_repository())
		#item = pywikibot.ItemPage(repo,'Q26704159')
		item.editEntity(data)
			
		upe = pywikibot.ItemPage(repo, 'Q4022')
		latvija = pywikibot.ItemPage(repo, 'Q211')
			
		simpleAdd(item,repo,'P31',upe)
		simpleAdd(item,repo,'P17',latvija)
			
if __name__ == "__main__":
	main()