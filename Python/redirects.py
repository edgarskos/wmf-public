#!/usr/bin/env python
'''
Creates various redirects
'''

import sys
import pywikibot

site = pywikibot.Site()
REDIRECT = '#REDIRECT [[%s]]'
def create(year):
	target = pywikibot.Page(site, str(year)+'. gada Eirovīzijas dziesmu konkurss')
	target2 = str(year)+'. gada Eirovīzijas dziesmu konkurss'
	if target.exists():
		pg1 = pywikibot.Page(site, 'ESC '+str(year))
		pg2 = pywikibot.Page(site, str(year)+'. gada Eirovīzija')
		pg3 = pywikibot.Page(site, 'Eirovīzija '+str(year))
		pg4 = pywikibot.Page(site, 'Eirovīzijas dziesmu konkurss '+str(year))
		pg5 = pywikibot.Page(site, 'Eurovision Song Contest '+str(year))
		for page in [pg1, pg2, pg3, pg4, pg5]:
			if page.exists():
				continue
			#print('Creating %s' % page.title(asLink=True))
			page.put(REDIRECT % target2, 'Pāradresē uz [[%s]]' % target2)
    
def main():
    year = 1950
    while year < 2020:
        create(year)
        year += 1
    
if __name__ == "__main__":
    pywikibot.handleArgs()
    main()