import requests, pywikibot, re, json, urllib.parse

file = open('chess-out2-3.txt','w', encoding='utf-8')
didntfound = open('chess-out-didntfound.txt','w', encoding='utf-8')

####################################################################json

QUERY = """SELECT ?item ?value
WHERE
{
	?item wdt:P1440 ?value .
}
"""

query2 = urllib.parse.quote(QUERY)

url = "https://query.wikidata.org/bigdata/namespace/wdq/sparql?query={}&format=json".format(query2)
url2= requests.get(url)

json_data = json.loads(url2.text)

itemlist = [[data['item']['value'].replace('http://www.wikidata.org/entity/',''),data['value']['value']] for data in json_data['results']['bindings']]

####################################################################

"""off-line version
####################################################################json

jsonfile = open("chess.json", "r", encoding='utf-8')

jsonfile = jsonfile.read()

json_data = json.loads(jsonfile)

itemlist = [[data['item'].replace('http://www.wikidata.org/entity/',''),data['value']] for data in json_data]

####################################################################
"""
####################################################################tsv
fideratingfile = open("chess-tsv.tsv", "r", encoding='utf-8').read()

fideratingfile = fideratingfile.split('\n')

fideratingfile2 = [f for f in fideratingfile if len(f)>0]
fideratingfile3 = [f.split('\t') for f in fideratingfile2]
fideratings = {f[0]:f[1] for f in fideratingfile3}

####################################################################tsv

for player in itemlist:
	wditem = player[0]
	fideid = player[1]
	
	if fideid not in fideratings:
		pywikibot.output('did not found id: ' +fideid+' from WD item: ' + wditem)
		didntfound.write("{}\t{}\n".format(wditem,fideid))
		continue
		
	rating = fideratings[fideid]
	
	#Q4115189 P1087 +2652 P585 +2016-09-20T00:00:00Z/11 S248 Q23058744 S813 +2016-09-20T00:00:00Z/11
	#outputstring = "{}\tP1087\t+{}\tP585\t+2016-09-20T00:00:00Z/11\tS248\tQ23058744\tS813\t+2016-09-20T00:00:00Z/11".format(wditem,rating)
	outputstring = "{}\t{}\t{}".format(wditem,fideid,rating)
	
	file.write(outputstring+'\n')
	
print('Done')