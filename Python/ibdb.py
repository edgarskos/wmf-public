import pywikibot

PROP = "P1220"

site = pywikibot.Site("en", "wikipedia")
article = "Chaim Topol"
page = pywikibot.Page(site,article)
repo = site.data_repository()

item = pywikibot.ItemPage.fromPage(page)
item.get()

if PROP in item.claims:
	values = [wdClaimValue.getTarget().title() for wdClaimValue in item.claims[PROP]]
	pywikibot.output(values)