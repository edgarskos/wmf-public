import pywikibot, re, time

site = pywikibot.Site("wikidata", "wikidata")
repo = site.data_repository()

text = """
 wd:Q134720	Martina Hingis		80301/title/martina-hingis
 wd:Q11662	Steffi Graf		
 wd:Q131120	Anna Kournikova		
 wd:Q54545	Martina Navratilova		
 wd:Q180872	Amélie Mauresmo		5394/title/amelie-mauresmo
 wd:Q11685	Kim Clijsters		
 wd:Q30758	Ana Ivanovic		311710/title/ana-ivanovic
 wd:Q180446	Li Na		
 wd:Q11682	Justine Henin		80350/title/justine-henin
 wd:Q173633	Monica Seles		190141/title/monica-seles
"""

prop = "P597"

def makewd(itemlist,file_map):
	counter = 0
	for item in itemlist:
		if item not in file_map:
			continue
		
		counter += 1
		if counter % 25 ==0:
			print(counter)
		itemT = item
		item = pywikibot.ItemPage(repo,item)
	
		item_dict = item.get()
		clm_dict = item_dict["claims"]
		claimsC = clm_dict[prop]
		
		for claim in claimsC:
			target = claim.getTarget()
			oldvalue = str(target)
            
			if oldvalue.isdigit():
				newvalue = file_map[itemT]
				target = newvalue
				if oldvalue != target:
					try:
						claim.changeTarget(target)
					except pywikibot.data.api.APIError:
						time.sleep(10)
						print('sleeping')
#
def main():
	newtext = [t.split('\t') for t in text.split('\n') if len(t)>2]
	
	items = []
	mas = {}
	
	for entry in newtext:
		#pywikibot.output(entry)
		wditem,_,_,id = entry
		wditem = wditem.strip().replace('wd:','')
		items.append(wditem)
		if not id.strip()=='':
			
			mas.update({wditem:id.strip()})
		
	makewd(items,mas)
#
main()