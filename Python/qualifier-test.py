import pywikibot

site = pywikibot.Site("wikidata", "wikidata")
repo = site.data_repository()

prop ="P2656"
itemlist = ['Q134916']

def getQualifiers(item):
	qualifiers = []
	
	fifa = item.claims[prop]
	
	for fifar in fifa:
		qualif = fifar.qualifiers['P585'][0].getTarget()
		if qualif:
			value = str(qualif.year)+str(qualif.month)+str(qualif.day)
			qualifiers.append(value)
			pywikibot.output(value)
			
	return qualifiers

for item in itemlist:
	item = pywikibot.ItemPage(repo, item)
	item.get()
	
	qualif = getQualifiers(item)
	
	if '2016915' in qualif:
		print('already added')