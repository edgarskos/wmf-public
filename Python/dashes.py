#!/usr/bin/python
# -*- coding: utf-8  -*-

import pywikibot
import re
import json
import urllib.request
import codecs

site = pywikibot.Site("lv", "wikipedia")
REDIRECT = '#REDIRECT [[%s]]'

url = "https://quarry.wmflabs.org/run/93098/output/0/json"
response = urllib.request.urlopen(url).read().decode('utf8')
reader = codecs.getreader("utf-8")
data = json.loads(response)

def create():
	for row in data["rows"]:
		article = row[0]
		target = pywikibot.Page(site, str(article))
		target2 = article
		if target.exists():
			pg1 = re.sub('—', '-', target2)
			target2 = re.sub('_', ' ', target2)
			page2 = pywikibot.Page(site, pg1)
			if page2.exists():
				print('Lapa "%s" jau eksistē' % page2.title(asLink=False))
				continue
			print('Creating %s' % page2.title(asLink=True))
			page2.put(REDIRECT % target2, 'Pāradresē uz [[%s]] no nosaukuma ar defisi' % target2)
			
if __name__ == "__main__":
    pywikibot.handleArgs()
    create()