import sys
import re
import csv
import urllib
import pywikibot
import datetime
from pywikibot import xmlreader
from pywikibot import textlib
import mwparserfromhell
from collections import Counter

starttimetime = datetime.datetime.now().time()
starttime = starttimetime.isoformat()

file = open("articleScanLV.txt", "w", encoding='utf-8')

array = []

def upperfirst(x):
	return x[0].upper() + x[1:]

for page in xmlreader.XmlDump(sys.argv[1]).parse():
	if page.ns == "0" and not page.isredirect:
		pagetext = textlib.unescape(page.text)
		wikicode = mwparserfromhell.parse(pagetext)
		templates = wikicode.filter_templates()
		
		for template in templates:
			if len(template.name)>0:
				name = upperfirst(template.name)
				name = name.replace('\n','').replace('_',' ')
				if name.endswith(' '):
					name = name[:-1]
				if name.startswith('#') or name.startswith('DEFAULTSORT'):
					continue
				pywikibot.output('%s-%s' % (page.title, name))
				array.append(name)

c = Counter(array)

for i in c:
    stringFFF = '{}\t{}\n'.format(i,c[i])
    file.write(stringFFF)
	
endtimetime = datetime.datetime.now().time()
endtime = endtimetime.isoformat()

pywikibot.output(starttime)
pywikibot.output(endtime)