"""
python query_wikidata_url_on_wikipedia.py > results.txt
"""
import MySQLdb
import sys
import settings

wikis = [u'abwiki',u'acewiki',u'adywiki',u'afwiki',u'akwiki',u'alswiki',u'amwiki',u'angwiki',u'anwiki',u'arcwiki',u'arwiki',u'arzwiki',u'astwiki',u'aswiki',u'avwiki',u'aywiki',u'azbwiki',u'azwiki',u'barwiki',u'bat_smgwiki',u'bawiki',u'bclwiki',u'be_x_oldwiki',u'bewiki',u'bgwiki',u'bhwiki',u'biwiki',u'bjnwiki',u'bmwiki',u'bnwiki',u'bowiki',u'bpywiki',u'brwiki',u'bswiki',u'bugwiki',u'bxrwiki',u'cawiki',u'cbk_zamwiki',u'cdowiki',u'cebwiki',u'cewiki',u'chrwiki',u'chwiki',u'chywiki',u'ckbwiki',u'cowiki',u'crhwiki',u'crwiki',u'csbwiki',u'cswiki',u'cuwiki',u'cvwiki',u'cywiki',u'dawiki',u'dewiki',u'diqwiki',u'dsbwiki',u'dtywiki',u'dvwiki',u'dzwiki',u'eewiki',u'elwiki',u'emlwiki',u'enwiki',u'eowiki',u'eswiki',u'etwiki',u'euwiki',u'extwiki',u'fawiki',u'ffwiki',u'fiu_vrowiki',u'fiwiki',u'fjwiki',u'fowiki',u'frpwiki',u'frrwiki',u'frwiki',u'furwiki',u'fywiki',u'gagwiki',u'ganwiki',u'gawiki',u'gdwiki',u'glkwiki',u'glwiki',u'gnwiki',u'gomwiki',u'gotwiki',u'guwiki',u'gvwiki',u'hakwiki',u'hawiki',u'hawwiki',u'hewiki',u'hifwiki',u'hiwiki',u'hrwiki',u'hsbwiki',u'htwiki',u'huwiki',u'hywiki',u'iawiki',u'idwiki',u'iewiki',u'igwiki',u'ikwiki',u'ilowiki',u'iowiki',u'iswiki',u'itwiki',u'iuwiki',u'jamwiki',u'jawiki',u'jbowiki',u'jvwiki',u'kaawiki',u'kabwiki',u'kawiki',u'kbdwiki',u'kgwiki',u'kiwiki',u'kkwiki',u'klwiki',u'kmwiki',u'knwiki',u'koiwiki',u'kowiki',u'krcwiki',u'kshwiki',u'kswiki',u'kuwiki',u'kvwiki',u'kwwiki',u'kywiki',u'ladwiki',u'lawiki',u'lbewiki',u'lbwiki',u'lezwiki',u'lgwiki',u'lijwiki',u'liwiki',u'lmowiki',u'lnwiki',u'lowiki',u'lrcwiki',u'ltgwiki',u'ltwiki',u'lvwiki',u'maiwiki',u'map_bmswiki',u'mdfwiki',u'mgwiki',u'mhrwiki',u'minwiki',u'miwiki',u'mkwiki',u'mlwiki',u'mnwiki',u'mrjwiki',u'mrwiki',u'mswiki',u'mtwiki',u'mwlwiki',u'myvwiki',u'mywiki',u'mznwiki',u'nahwiki',u'napwiki',u'nawiki',u'nds_nlwiki',u'ndswiki',u'newiki',u'newwiki',u'nlwiki',u'nnwiki',u'novwiki',u'nowiki',u'nrmwiki',u'nsowiki',u'nvwiki',u'nywiki',u'ocwiki',u'olowiki',u'omwiki',u'orwiki',u'oswiki',u'pagwiki',u'pamwiki',u'papwiki',u'pawiki',u'pcdwiki',u'pdcwiki',u'pflwiki',u'pihwiki',u'piwiki',u'plwiki',u'pmswiki',u'pnbwiki',u'pntwiki',u'pswiki',u'ptwiki',u'quwiki',u'rmwiki',u'rmywiki',u'rnwiki',u'roa_rupwiki',u'roa_tarawiki',u'rowiki',u'ruewiki',u'ruwiki',u'rwwiki',u'sahwiki',u'sawiki',u'scnwiki',u'scowiki',u'scwiki',u'sdwiki',u'sewiki',u'sgwiki',u'shwiki',u'simplewiki',u'siwiki',u'skwiki',u'slwiki',u'smwiki',u'snwiki',u'sowiki',u'sqwiki',u'srnwiki',u'srwiki',u'sswiki',u'stqwiki',u'stwiki',u'suwiki',u'svwiki',u'swwiki',u'szlwiki',u'tawiki',u'tcywiki',u'test2wiki',u'testwiki',u'tetwiki',u'tewiki',u'tgwiki',u'thwiki',u'tiwiki',u'tkwiki',u'tlwiki',u'tnwiki',u'towiki',u'tpiwiki',u'trwiki',u'tswiki',u'ttwiki',u'tumwiki',u'twwiki',u'tyvwiki',u'tywiki',u'udmwiki',u'ugwiki',u'ukwiki',u'urwiki',u'uzwiki',u'vecwiki',u'vepwiki',u'vewiki',u'viwiki',u'vlswiki',u'vowiki',u'warwiki',u'wawiki',u'wowiki',u'wuuwiki',u'xalwiki',u'xhwiki',u'xmfwiki',u'yiwiki',u'yowiki',u'zawiki',u'zeawiki',u'zh_classicalwiki',u'zh_min_nanwiki',u'zh_yuewiki',u'zhwiki',u'zuwiki']

#wikis = [u'abwiki_p',u'acewiki_p',u'adywiki_p',u'afwiki_p',u'akwiki_p',u'alswiki_p',u'amwiki_p',u'angwiki_p',u'anwiki_p',u'arcwiki_p',u'arwiki_p',u'arzwiki_p',u'astwiki_p',u'aswiki_p',u'avwiki_p',u'aywiki_p',u'azbwiki_p',u'azwiki_p',u'barwiki_p',u'bat_smgwiki_p',u'bawiki_p',u'bclwiki_p',u'be_x_oldwiki_p',u'bewiki_p',u'bgwiki_p',u'bhwiki_p',u'biwiki_p',u'bjnwiki_p',u'bmwiki_p',u'bnwiki_p',u'bowiki_p',u'bpywiki_p',u'brwiki_p',u'bswiki_p',u'bugwiki_p',u'bxrwiki_p',u'cawiki_p',u'cbk_zamwiki_p',u'cdowiki_p',u'cebwiki_p',u'cewiki_p',u'chrwiki_p',u'chwiki_p',u'chywiki_p',u'ckbwiki_p',u'cowiki_p',u'crhwiki_p',u'crwiki_p',u'csbwiki_p',u'cswiki_p',u'cuwiki_p',u'cvwiki_p',u'cywiki_p',u'dawiki_p',u'dewiki_p',u'diqwiki_p',u'dsbwiki_p',u'dtywiki_p',u'dvwiki_p',u'dzwiki_p',u'eewiki_p',u'elwiki_p',u'emlwiki_p',u'enwiki_p',u'eowiki_p',u'eswiki_p',u'etwiki_p',u'euwiki_p',u'extwiki_p',u'fawiki_p',u'ffwiki_p',u'fiu_vrowiki_p',u'fiwiki_p',u'fjwiki_p',u'fowiki_p',u'frpwiki_p',u'frrwiki_p',u'frwiki_p',u'furwiki_p',u'fywiki_p',u'gagwiki_p',u'ganwiki_p',u'gawiki_p',u'gdwiki_p',u'glkwiki_p',u'glwiki_p',u'gnwiki_p',u'gomwiki_p',u'gotwiki_p',u'guwiki_p',u'gvwiki_p',u'hakwiki_p',u'hawiki_p',u'hawwiki_p',u'hewiki_p',u'hifwiki_p',u'hiwiki_p',u'hrwiki_p',u'hsbwiki_p',u'htwiki_p',u'huwiki_p',u'hywiki_p',u'iawiki_p',u'idwiki_p',u'iewiki_p',u'igwiki_p',u'ikwiki_p',u'ilowiki_p',u'iowiki_p',u'iswiki_p',u'itwiki_p',u'iuwiki_p',u'jamwiki_p',u'jawiki_p',u'jbowiki_p',u'jvwiki_p',u'kaawiki_p',u'kabwiki_p',u'kawiki_p',u'kbdwiki_p',u'kgwiki_p',u'kiwiki_p',u'kkwiki_p',u'klwiki_p',u'kmwiki_p',u'knwiki_p',u'koiwiki_p',u'kowiki_p',u'krcwiki_p',u'kshwiki_p',u'kswiki_p',u'kuwiki_p',u'kvwiki_p',u'kwwiki_p',u'kywiki_p',u'ladwiki_p',u'lawiki_p',u'lbewiki_p',u'lbwiki_p',u'lezwiki_p',u'lgwiki_p',u'lijwiki_p',u'liwiki_p',u'lmowiki_p',u'lnwiki_p',u'lowiki_p',u'lrcwiki_p',u'ltgwiki_p',u'ltwiki_p',u'lvwiki_p',u'maiwiki_p',u'map_bmswiki_p',u'mdfwiki_p',u'mgwiki_p',u'mhrwiki_p',u'minwiki_p',u'miwiki_p',u'mkwiki_p',u'mlwiki_p',u'mnwiki_p',u'mrjwiki_p',u'mrwiki_p',u'mswiki_p',u'mtwiki_p',u'mwlwiki_p',u'myvwiki_p',u'mywiki_p',u'mznwiki_p',u'nahwiki_p',u'napwiki_p',u'nawiki_p',u'nds_nlwiki_p',u'ndswiki_p',u'newiki_p',u'newwiki_p',u'nlwiki_p',u'nnwiki_p',u'novwiki_p',u'nowiki_p',u'nrmwiki_p',u'nsowiki_p',u'nvwiki_p',u'nywiki_p',u'ocwiki_p',u'olowiki_p',u'omwiki_p',u'orwiki_p',u'oswiki_p',u'pagwiki_p',u'pamwiki_p',u'papwiki_p',u'pawiki_p',u'pcdwiki_p',u'pdcwiki_p',u'pflwiki_p',u'pihwiki_p',u'piwiki_p',u'plwiki_p',u'pmswiki_p',u'pnbwiki_p',u'pntwiki_p',u'pswiki_p',u'ptwiki_p',u'quwiki_p',u'rmwiki_p',u'rmywiki_p',u'rnwiki_p',u'roa_rupwiki_p',u'roa_tarawiki_p',u'rowiki_p',u'ruewiki_p',u'ruwiki_p',u'rwwiki_p',u'sahwiki_p',u'sawiki_p',u'scnwiki_p',u'scowiki_p',u'scwiki_p',u'sdwiki_p',u'sewiki_p',u'sgwiki_p',u'shwiki_p',u'simplewiki_p',u'siwiki_p',u'skwiki_p',u'slwiki_p',u'smwiki_p',u'snwiki_p',u'sowiki_p',u'sqwiki_p',u'srnwiki_p',u'srwiki_p',u'sswiki_p',u'stqwiki_p',u'stwiki_p',u'suwiki_p',u'svwiki_p',u'swwiki_p',u'szlwiki_p',u'tawiki_p',u'tcywiki_p',u'test2wiki_p',u'testwiki_p',u'tetwiki_p',u'tewiki_p',u'tgwiki_p',u'thwiki_p',u'tiwiki_p',u'tkwiki_p',u'tlwiki_p',u'tnwiki_p',u'towiki_p',u'tpiwiki_p',u'trwiki_p',u'tswiki_p',u'ttwiki_p',u'tumwiki_p',u'twwiki_p',u'tyvwiki_p',u'tywiki_p',u'udmwiki_p',u'ugwiki_p',u'ukwiki_p',u'urwiki_p',u'uzwiki_p',u'vecwiki_p',u'vepwiki_p',u'vewiki_p',u'viwiki_p',u'vlswiki_p',u'vowiki_p',u'warwiki_p',u'wawiki_p',u'wowiki_p',u'wuuwiki_p',u'xalwiki_p',u'xhwiki_p',u'xmfwiki_p',u'yiwiki_p',u'yowiki_p',u'zawiki_p',u'zeawiki_p',u'zh_classicalwiki_p',u'zh_min_nanwiki_p',u'zh_yuewiki_p',u'zhwiki_p',u'zuwiki_p']

first_list = [
	[u'P3314',u'http://www.365chess.com/players/%'],
	[u'P3605',u'http://www.90minut.pl/kariera.php?id=%'],
	[u'P3525',u'http://www.acb.com/jugador.php?id=%'],
	[u'P3547',u'http://afltables.com/afl/stats/players%'],
	[u'P3651',u'http://theahl.com/stats/player/%'],
	[u'P3427',u'http://resultados.as.com/resultados/ficha/deportista/%'],
	[u'P536',u'http://www.atpworldtour.com/en/players%'],
	[u'P3456',u'http://www.atpworldtour.com/en/tournaments/%'],
	[u'P3682',u'http://corporate.olympics.com.au/athlete/%'],
	[u'P3546',u'http://australianfootball.com/players/player/%'],
	[u'P3531',u'http://www.azbilliards.com/people/%'],
	[u'P2729',u'http://www.tournamentsoftware.com/profile/overview.aspx?id=%'],
	[u'P3623',u'http://www.badmintonlink.com/Player-Info/%'],
	[u'P1825',u'http://www.baseball-reference.com/players%'],
	[u'P1826',u'http://www.baseball-reference.com/minors/player%'],
	[u'P3646',u'http://www.hoophall.com/hall-of-famers/%'],
	[u'P2685',u'http://www.basketball-reference.com/players/%'],
	[u'P3655',u'http://www.bdfutbol.com/en/j%'],
	[u'P2800',u'http://www.bvbinfo.com/player.asp%'],
	[u'P1967',u'http://boxrec.com/boxer/%'],
	[u'P3620',u'http://bwfbadminton.com/player/%'],
	[u'P2939',u'http://www.cagematch.net/?id=28&nr=%'],
	[u'P2728',u'http://www.cagematch.net/?id=%'],
	[u'P3042',u'http://www.cagematch.net/?id=29&nr=%'],
	[u'P3725',u'http://www.cev.lu/PlayerDetail.aspx?PlayerID=%'],
	[u'P1666',u'http://www.chessclub.com/finger/%'],
	[u'P1665',u'http://www.chessgames.com/perl/chessplayer?pid=%'],
	[u'P3654',u'http://www.chess.com/members/view/%'],
	[u'P3315',u'http://chesstempo.com/gamedb/player/%'],
	[u'P3560',u'http://www.cfbdatawarehouse.com/data/coaching/alltime_coach_year_by_year.php?coachid=%'],
	[u'P3044',u'http://www.footballfoundation.org/Programs/CollegeFootballHallofFame/SearchDetail.aspx?id=%'],
	[u'P3586',u'http://www.cricketarchive.com/Archive/Grounds/%'],
	[u'P3577',u'http://hns-cff.hr/en/players/%'],
	[u'P3657',u'http://hrnogomet.com/hnl/igrac.php?id=%'],
	[u'P3830',u'http://cuetracker.net/Players/%'],
	[u'P1409',u'http://www.cyclingarchives.com/coureurfiche.php?coureurid=%'],
	[u'P2330',u'http://www.cykelsiderne.net/ritfiche.php?ritid=%'],
	[u'P2331',u'http://www.cykelsiderne.net/ploegfiche.php?id=%'],
	[u'P1664',u'https://www.cyclingdatabase.com/%'],
	[u'P2709',u'http://www.cqranking.com/women/asp/gen/rider.asp?riderid=%'],
	[u'P2648',u'http://www.cqranking.com/men/asp/gen/race.asp?raceid=%'],
	[u'P1541',u'http://www.cqranking.com/men/asp/gen/rider.asp%'],
	[u'P2708',u'http://www.cqranking.com/women/asp/gen/race.asp?raceid=%'],
	[u'P3621',u'http://www.dartsdatabase.co.uk/PlayerDetails.aspx?playerKey=%'],
	[u'P3532',u'http://www.databasefootball.com/players/playerpage.htm?ilkid=%'],
	[u'P3520',u'http://www.databaseolympics.com/players/playerpage.htm?ilkid=%'],
	[u'P2641',u'http://www.daviscup.com/en/players/player.aspx?id=%'],
	[u'P3533',u'http://www.draftexpress.com/profile/%'],
	[u'P3684',u'https://www.driverdb.com/drivers/%'],
	[u'P3658',u'http://www.dzfoot.com/fiche-joueur?id=%'],
	[u'P2481',u'http://www.eliteprospects.com/player.php?player=%'],
	[u'P3338',u'http://encyclopediaofsurfing.com/entries/%'],
	[u'P3571',u'http://www.espn.com/mlb/player/stats/%'],
	[u'P3685',u'http://www.espn.com/nba/player%'],
	[u'P3686',u'http://www.espn.com/nfl/player/stats/%'],
	[u'P3687',u'http://www.espn.com/nhl/player/%'],
	[u'P3572',u'http://www.espncricinfo.com/ci/content/ground/%'],
	#[u'P2697',u'http://www.espncricinfo.com/ci/content/player/%'],
	[u'P3681',u'http://www.espnfc.com/player/%'],
	[u'P3659',u'http://www.jalgpall.ee/players.php%'],
	[u'P3726',u'http://eu-football.info/_player.php?id=%'],
	[u'P3527',u'http://www.eurobasket.com/player.asp?PlayerID=%'],
	[u'P2601',u'http://www.eurohockey.com/player/%'],
	[u'P3536',u'http://www.euroleague.net/competition/players%'],
	[u'P3573',u'http://www.eurohandball.com/player/%'],
	[u'P3521',u'http://www.europeantour.com/europeantour/players/playerid=%'],
	[u'P3050',u'http://nv.fotbal.cz/reprezentace/reprezentace-a/statistiky/viewstat3.asp?name=%'],
	[u'P3574',u'http://www.fangraphs.com/statss.aspx?playerid=%'],
	[u'P2642',u'http://www.fedcup.com/en/players/player.aspx?id=%'],
	[u'P3111',u'http://www.fei.org/bios/Person/%'],
	[u'P3542',u'http://www.fiba.com/pages/eng/fc/gamecent/p/pid/%'],
	[u'P1440',u'https://ratings.fide.com/card.phtml?event=$1%'],
	[u'P2423',u'http://fie.org/fencers/fencer/%'],
	[u'P2696',u'https://database.fig-gymnastics.com/public%'],
	[u'P3742',u'http://www.fih.ch/global-stars/profiles/%'],
	[u'P2990',u'http://www.fil-luge.org/en/athletes/%'],
	[u'P3408',u'http://www.fina.org/athletes/%'],
	[u'P2091',u'http://www.worldrowing.com/athletes/athlete/%'],
	[u'P2801',u'http://www.fivb.org/EN/BeachVolleyball/Player_DataDB.asp?No=%'],
	[u'P3662',u'http://www.ffu.org.ua/ukr/tournaments/prof/%'],
	[u'P3537',u'http://www.footballdatabase.eu/football.joueur%'],
	[u'P3660',u'http://footballfacts.ru/players/%'],
	[u'P3661',u'https://www.foradejogo.net/manager.php%'],
	[u'P3046',u'http://www.foradejogo.net/player.php?player=%'],
	[u'P3663',u'http://fotbal.idnes.cz/databanka.aspx?t=hrac&id=%'],
	[u'P3538',u'http://www.fussballdaten.de/spieler/%'],
	[u'P3664',u'http://www.futsalplanet.com/gallery/gallery-02.asp%'],
	[u'P2805',u'http://www.goratings.org/players/%'],
	[u'P3567',u'http://www.hhof.com/LegendsOfHockey/jsp/SearchPlayer.jsp?player=%'],
	[u'P2602',u'http://www.hockeydb.com/ihdb/stats/pdisplay.php?pid=%'],
	[u'P3598',u'http://www.hockey-reference.com/players/%'],
	[u'P2780',u'https://www.iat.uni-leipzig.de/datenbanken/dbdiving/daten.php?id_sportler=%'],
	[u'P2778',u'https://www.iat.uni-leipzig.de/datenbanken/dbtriathlon/daten.php?spid=%'],
	[u'P2779',u'https://www.iat.uni-leipzig.de/datenbanken/dbgwh/daten.php?spid=%'],
	[u'P2991',u'http://www.ibsf.org/en/athletes/athlete/%'],
	[u'P2459',u'http://services.biathlonresults.com/athletes.aspx?IbuId=%'],
	[u'P3316',u'https://www.iccf.com/player?id=%'],
	[u'P3689',u'http://icf.html.infostradasports.com/asp/redirect/icf.asp%'],
	[u'P3688',u'http://icf.html.infostradasports.com/asp/redirect/icf.asp%'],
	[u'P3760',u'http://iditarod.com/race/mushers/%'],
	[u'P3690',u'http://www.ifsc-climbing.org/index.php?option=com_ifsc&view=athlete&id=%'],
	[u'P3171',u'https://www.olympic.org/%'],
	[u'P3667',u'http://www.iwf.net/results/athletes/?athlete=&id=%'],
	[u'P2829',u'http://www.profightdb.com/wrestlers%'],
	[u'P3691',u'http://www.ishof.org/%'],
	[u'P3867',u'http://www.chess.org.il/Players/Player.aspx?Id=%'],
	[u'P3748',u'http://football.org.il/NationalTeam/Pages/NationalTeamPlayerDetails.aspx?PLAYER_ID=%'],
	[u'P2730',u'http://www.issf-sports.org/athletes/athlete.ashx?personissfid=%'],
	[u'P2694',u'http://www.isuresults.com/bios/%'],
	[u'P599',u'http://www.itftennis.com/procircuit/players/player/profile.aspx?playerid=%'],
	[u'P1364',u'http://old.ittf.com/ittf_ranking/WR_Table_3_A2_Details.asp?Player_ID=%'],
	[u'P3604',u'http://www.triathlon.org/athletes/profile/%'],
	[u'P3565',u'https://data.j-league.or.jp/SFIX04/?player_id=%'],
	[u'P3385',u'http://www.sumo.or.jp/EnSumoDataRikishi/profile?id=%'],
	[u'P3535',u'http://www.jgto.org/pc/WG05020200Profile.do?playerCd=%'],
	[u'P2767',u'http://www.judoinside.com/judoka/%'],
	[u'P3566',u'http://www.justsportsstats.com/footballstatsindex.php?player_id=%'],
	[u'P3053',u'http://eng.kleague.com/eng/sub.asp%'],
	[u'P2705',u'http://www.karaterec.com%'],
	[u'P3897',u'http://ladieseuropeantour.com/player/?player=%'],
	[u'P2593',u'http://olimpiade.lv%'],
	[u'P3665',u'http://www.lequipe.fr/Football/FootballFicheJoueur%'],
	[u'P3683',u'http://www.lfp.fr/joueur/%'],
	[u'P2810',u'http://www.lpga.com/players/%'],
	[u'P2458',u'http://www.mackolik.com/Player/Default.aspx?id=%'],
	[u'P3541',u'http://mlb.mlb.com/team/player.jsp?player_id=%'],
	[u'P2398',u'http://www.mlssoccer.com/players/%'],
	[u'P1285',u'http://www.munzinger.de/search/go/document.jsp?id=%'],
	[u'P3647',u'http://www.nba.com/players/%'],
	[u'P3692',u'http://www.ncaa.com/schools/%'],
	[u'P3539',u'http://www.nfl.com/playe%'],
	[u'P3700',u'https://handballold.nif.no/Statistikk_Landskamper.asp?SpillerId=%'],
	[u'P3522',u'http://www.nhl.com/ice/player.htm?id=%'],
	[u'P3715',u'http://www.wp12060650.server-he.de/index.php/hall-of-fame.html?person=%'],
	[u'P2830',u'http://www.onlineworldofwrestling.com/bios/%'],
	[u'P3568',u'http://www.owgr.com/en/Ranking/PlayerProfile.aspx?playerID=%'],
	[u'P2811',u'http://www.pgatour.com/players/player%'],
	[u'P3581',u'http://www.profootballhof.com/hof/member.aspx?PlayerId=%'],
	[u'P1663',u'http://www.procyclingstats.com/rider/%'],
	[u'P2327',u'http://www.procyclingstats.com/race/%'],
	[u'P2328',u'http://www.procyclingstats.com/team/%'],
	[u'P3561',u'http://www.pro-football-reference.com/players/%'],
	[u'P3476',u'https://psaworldtour.com/players/view/%'],
	[u'P3048',u'http://www.racing-reference.info/driver/%'],
	[u'P3883',u'http://www.redbull.com/en/athletes/%'],
	[u'P2823',u'http://static.belgianfootball.be/project/publiek/jrinteren/speler_PH_%'],
	[u'P3622',u'http://www.rusbandy.ru/gamer/%'],
	[u'P2482',u'http://sabr.org/bioproj/person/%'],
	[u'P3800',u'http://www.safsal.co.il/hstaff.aspx?id=%'],
	[u'P3799',u'http://www.safsal.co.il/hplayer.aspx?id=%'],
	[u'P3668',u'http://www.sambafoot.com/en/players/%'],
	#[u'P3043',u'http://www.scoresway.com/?sport=soccer&page=person&id=%'],
	[u'P3049',u'http://www.scottishfa.co.uk/player_details.cfm?playerid=%'],
	[u'P2818',u'http://www.sherdog.com/fightfinder/fightfinder.asp?FighterID=%'],
	[u'P3693',u'http://www.shorttrackonline.info/skaterbio.php?id=%'],
	[u'P3619',u'http://www.ski-db.com/db/profiles/%'],
	[u'P2195',u'http://www.soccerbase.com/managers/manager.sd?manager_id=%'],
	[u'P2193',u'http://www.soccerbase.com/players/player.sd?player_id=%'],
	[u'P2350',u'http://www.speedskatingbase.eu/?section=skaters&subsection=skater&skaterid=%'],
	[u'P3694',u'http://www.speedskatingnews.info/en/data/skater/%'],
	[u'P3695',u'http://www.schaatsstatistieken.nl/index.php?file=schaatser&code=%'],
	[u'P1447',u'http://www.sports-reference.com/olympics/athletes/%'],
	[u'P3286',u'http://www.squashinfo.com/players/%'],
	[u'P3696',u'http://www.sports-reference.com/cbb/players/%'],
	[u'P3697',u'http://www.sports-reference.com/cfb/players/%'],
	[u'P3582',u'http://www.sunshinetour.info/profiles/profile.php?player=%'],
	[u'P3583',u'http://www.surfline.com/surfaz/surfaz.cfm?id=%'],
	[u'P1238',u'http://svenskfotboll.se/allsvenskan/person/?playerid=%'],
	[u'P2323',u'http://sok.se/idrottare/idrottare%'],
	[u'P3669',u'http://sal-live.aptsolutions.net/article.php?group_id=%'],
	[u'P2640',u'https://www.swimrankings.net/index.php?page=athleteDetail&athleteId=%'],
	[u'P2987',u'http://www.taekwondodata.com/%'],
	[u'P3670',u'http://www.tennisarchives.com/player.php?playerid=%'],
	[u'P3698',u'http://www.tennis.com.au/player-profiles/%'],
	[u'P3363',u'https://www.tennisfame.com/hall-of-famers/inductees/%'],
	[u'P3047',u'http://www.zerozero.pt/player.php?id=%'],
	[u'P2449',u'http://www.tff.org/Default.aspx?pageID=527&antID=%'],
	[u'P2448',u'http://www.tff.org/Default.aspx?pageId=526&kisiID=%'],
	[u'P2726',u'http://www.uipmworld.org/athlete/%'],
	[u'P2727',u'https://www.iat.uni-leipzig.de/datenbanken/dbfoeldeak/daten.php?spid=%'],
	[u'P3671',u'http://usagym.org/pages/athletes/athleteListDetail.html?id=%'],
	[u'P3558',u'http://www.uschess.org/msa/MbrDtlMain.php?%'],
	[u'P3677',u'http://www.wereldvanoranje.nl/profielen/profiel.php?id=%'],
	[u'P3526',u'http://www.wisdenindia.com/player/%'],
	[u'P3588',u'http://www.wnba.com/player/%'],
	[u'P3010',u'https://worldarchery.org/athlete/%'],
	[u'P3557',u'http://results.worldcurling.org/Person/Details%'],
	[u'P3556',u'http://www.worldcurl.com/player.php?playerid=%'],
	[u'P2804',u'http://www.sailing.org/biog?memberid=%'],
	[u'P3339',u'http://www.worldsurfleague.com/athletes/%'],
	[u'P2020',u'http://www.worldfootball.net/player_summary/%'],
	[u'P2764',u'http://wrestlingdata.com/index.php?befehl=bios&wrestler=%'],
	[u'P597',u'http://www.wtatennis.com/players/player/%'],
	[u'P3469',u'http://www.wtatennis.com/tournaments/tournamentId/%'],
	[u'P2857',u'http://www.wwe.com/superstars/%'],
	#[u'P345',u'http://www.imdb.com/%'],
	[u'P3940',u'http://www.olimpbase.org/players/%'],
]

second_list = [
	[u'P2698',u'http://com.cricketarchive.%'],
	[u'P3652',u'http://ru.khl.%'],
	[u'P1469',u'http://com.fifa.%'],
	[u'P2574',u'http://com.national-football-teams.%'],
	[u'P2369',u'http://com.soccerway%'],
	[u'P3043',u'http://com.soccerway%'],
	[u'P2276',u'http://com.uefa.%'],
	[u'P2697',u'http://com.espncricinfo.%'],
	[u'P345',u'http://com.imdb.%'],
]
#
def main_run(main_list):
	for row in main_list:
		prop_title, url_formatter = row
		print (prop_title, url_formatter)
		for dbname in wikis:
			#here probably should be some try .. except
			conn = MySQLdb.connect(host=settings.host, db=dbname, read_default_file='~/.my.cnf')
			cursor = conn.cursor()
			cursor.execute("""
SELECT 
  database() AS dbname,
  wp.page_title AS Title,
  pp_value,
  el_to
FROM page AS wp
JOIN externallinks ON el_from = wp.page_id
LEFT JOIN page_props AS pp
  ON pp.pp_page = wp.page_id AND pp.pp_propname = 'wikibase_item'
LEFT JOIN wikidatawiki_p.page AS wdp
  ON wdp.page_title = pp.pp_value AND wdp.page_namespace = 0
LEFT JOIN wikidatawiki_p.pagelinks AS wdpl
  ON wdpl.pl_from = wdp.page_id AND wdpl.pl_namespace = 120 AND wdpl.pl_title = ?
WHERE wp.page_namespace = 0
AND   el_to     LIKE ?
AND   wdpl.pl_from IS NULL;
""", (prop_title, url_formatter,))
			if cursor.rowcount:
				print dbname, cursor.rowcount
				sys.stdout.flush()
				with codecs.open('results_%s_%s.tsv' % (dbname, prop_title), 'w', 'utf-8') as f:
					for dbname, title, q_item, el_to in cursor.fetchall():
						f.write(u'\t'.join((dbname, title, q_item, el_to))+u'\n')
			else:
				print dbname, u'0'
				sys.stdout.flush()
			
#
def second_run(main_list):
	for row in main_list:
		prop_title, url_formatter = row
		print (prop_title, url_formatter)
		for dbname in wikis:
			#here probably should be some try .. except
			conn = MySQLdb.connect(host=settings.host, db=dbname, read_default_file='~/.my.cnf')
			cursor = conn.cursor()
			cursor.execute("""
SELECT 
  database() AS dbname,
  wp.page_title AS Title,
  pp_value,
  el_to
FROM page AS wp
JOIN externallinks ON el_from = wp.page_id
LEFT JOIN page_props AS pp
  ON pp.pp_page = wp.page_id AND pp.pp_propname = 'wikibase_item'
LEFT JOIN wikidatawiki_p.page AS wdp
  ON wdp.page_title = pp.pp_value AND wdp.page_namespace = 0
LEFT JOIN wikidatawiki_p.pagelinks AS wdpl
  ON wdpl.pl_from = wdp.page_id AND wdpl.pl_namespace = 120 AND wdpl.pl_title = ?
WHERE wp.page_namespace = 0
AND   el_index     LIKE ?
AND   wdpl.pl_from IS NULL;
""", (prop_title, url_formatter,))
			if cursor.rowcount:
				print dbname, cursor.rowcount
				sys.stdout.flush()
				with codecs.open('results_%s_%s.tsv' % (dbname, prop_title), 'w', 'utf-8') as f:
					for dbname, title, q_item, el_to in cursor.fetchall():
						f.write(u'\t'.join((dbname, title, q_item, el_to))+u'\n')
			else:
				print dbname, u'0'
				sys.stdout.flush()
#


def fis():
	print u'FIS'
	for dbname in wikis:
		#here probably should be some try .. except
		conn = MySQLdb.connect(host=settings.host, db=dbname, read_default_file='~/.my.cnf')
		cursor = conn.cursor()
		cursor.execute("""
SELECT
  database() AS dbname,
  p.page_title AS Title,
  el_to,
  props,
  pp.pp_value
FROM (SELECT * FROM externallinks WHERE el_index LIKE "http://com.fis-ski.data./%" or el_index LIKE "http://com.fis-ski.www./uk/%") AS el
JOIN page AS p
	ON p.page_id = el_from
LEFT JOIN page_props AS pp
	ON pp.pp_page = p.page_id AND pp.pp_propname = 'wikibase_item'
LEFT JOIN wikidatawiki_p.page AS wdp
	ON wdp.page_title = pp.pp_value AND wdp.page_namespace = 0
LEFT JOIN (SELECT pl_from, GROUP_CONCAT( pl_title ) AS props FROM wikidatawiki_p.pagelinks
           WHERE pl_title IN ('P2772','P2773','P2774','P2776','P2775','P2777') AND pl_namespace = 120
           GROUP BY pl_from) AS wdpl
	ON wdpl.pl_from = wdp.page_id
WHERE p.page_namespace = 0;
""")
		if cursor.rowcount:
			print dbname, cursor.rowcount
			sys.stdout.flush()
			with codecs.open('results_%s_FIS.tsv' % (dbname), 'w', 'utf-8') as f:
				for dbname, title, el_to, props, q_item  in cursor.fetchall():
					f.write(u'\t'.join((dbname, title, el_to, props, q_item))+u'\n')
		else:
			print dbname, u'0'
			sys.stdout.flush()
#

def transfermarkt():
	print u'Transfermarkt'
	for dbname in wikis:
		#here probably should be some try .. except
		conn = MySQLdb.connect(host=settings.host, db=dbname, read_default_file='~/.my.cnf')
		cursor = conn.cursor()
		cursor.execute("""
SELECT
  database() AS dbname,
  p.page_title as Title,
  el_to,
  props,
  pp.pp_value
FROM (SELECT * FROM externallinks WHERE el_to LIKE "http://www.transfermarkt.com%") AS el
JOIN page AS p
	ON p.page_id = el_from
LEFT JOIN page_props AS pp
	ON pp.pp_page = p.page_id AND pp.pp_propname = 'wikibase_item'
LEFT JOIN wikidatawiki_p.page AS wdp
	ON wdp.page_title = pp.pp_value AND wdp.page_namespace = 0
LEFT JOIN (SELECT pl_from, GROUP_CONCAT( pl_title ) AS props FROM wikidatawiki_p.pagelinks
           WHERE pl_title IN ('P2447','P2446','P3699') AND pl_namespace = 120
           GROUP BY pl_from) AS wdpl
	ON wdpl.pl_from = wdp.page_id
WHERE p.page_namespace = 0;
""")
		if cursor.rowcount:
			print dbname, cursor.rowcount
			sys.stdout.flush()
			with codecs.open('results_%s_Transfer.tsv' % (dbname), 'w', 'utf-8') as f:
				for dbname, title, el_to, props, q_item in cursor.fetchall():
					f.write(u'\t'.join((dbname, title, el_to, props, q_item))+u'\n')
		else:
			print dbname, u'0'
			sys.stdout.flush()
#

def main():
	main_run(first_list)
	second_run(second_list)
	fis()
	transfermarkt()
#
main()