import re
import pywikibot
from pywikibot import xmlreader
from pywikibot import textlib

input_file = 'lvwiki-20160701-pages-articles.xml'
log = open('templates-by-invocations-in-articles-lvwiki-20160701.txt', 'w', encoding='utf-8')

template_re = re.compile(r'{{([^|}]+)')
namespace_id = None
text = []
loop = False
pages_processed = 0

def extract_templates(page_text):
    templates = []
    for line in page_text.split('\n'):
        for match in template_re.findall(line):
            match = match.strip()
            try:
                match = match[0].upper() + match[1:]
                templates.append(match)
            except IndexError:
                print(match)
    return templates

for page in xmlreader.XmlDump(input_file).parse():
	if page.ns == "0" and not page.isredirect:
		pagetext = textlib.unescape(page.text)
		templates = extract_templates(pagetext)
		if templates:
			for template in templates:
				if template.startswith('#') or template.startswith('DEFAULTSORT'):
					continue
				log.write(template + '\n')