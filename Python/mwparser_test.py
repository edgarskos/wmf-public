import time
from pywikibot import textlib


text = """
'''Mongolijas premjerministrs''' ({{val-mn|Монгол Улсын Ерөнхий Сайд}}) ir [[Mongolija]]s galvenā izpildvara. To iebalso [[Mongolijas Parlaments|Parlaments]] un tas var tikt atstādināts no amata pēc [[neuzticības balsojums|neuzticības balsojuma]].

== Premjerministri no 1912. gada līdz mūsdienām ==

<table border=0 cellpadding=2 cellspacing=2 width=90%>

<th>
<th> Premjers 
<th> Laiks
<th> Partija

<tr bgcolor=#EEEEEE>
<td>1.<td>[[Sažnožonkans Širidanbins Namansurens]]<br />''(Sajnnojonkhan Shirindambyn Namnansuren)''<td>1912.gada novembris - 1919.gada aprīlis<td>b.p.

<tr bgcolor=#EEEEEE>
<td>2.<td>[[Hatanbātars Sandagdoržijns Maksarjavs]]<br />''(Khatan-Baatar Sandagdorjiyn Maksarjav)''<td>1921.gada 15.februāris - 1921.gada 13.marts<td>b.p.

<tr bgcolor=#FFE8E8>
<td>3.<td>[[Dambijns Čadardžavs]] ''(Dambyn Chadarjav)''<td>2021.gada 13.marts - 1921.gada 16.aprīlis <td>TRP

<tr bgcolor=#FFE8E8>
<td>4.<td>[[Dogsomijns Bodoo]] ''(Dogsomyn Bodoo)''<td>1921.gada 16.aprīlis - 1922.gada 7.janvāris<td>TRP

<tr bgcolor=#FFE8E8>
<td>5.<td>[[Sodnomijns Damdinbazars]] ''(Sodnomyn Damdinbazar)''<td>1922.gada 3.marts - 1923.gada 23.jūnijs<td>TRP

<tr bgcolor=#FFE8E8>
<td>6.<td>[[Balingijns Cerendoržs Beijse]] ''(Balingiyn Tserendorj Beyse)''<td>1923.gada 28.septembris - 1928.gada 13.februaris<td>TRP

<tr bgcolor=#FFE8E8>
<td>7.<td>[[Agdanbūgijns Amars]] ''(Agdanbuugiyn Amar)''<td>1928.gada 21.februāris - 1930.gada 27.aprīlis<td>TRP

<tr bgcolor=#FFE8E8>
<td>8.<td>[[Cengeltijns Žigžidavs]] ''(Tsengeltiyn Jigjidav)''<td>1930.gada 27.aprīlis - 1932.gada 2.jūlijs<td>TRP

<tr bgcolor=#FFE8E8>
<td>9.<td>[[Pelžidijns Gendens]] ''(Peljidiyn Genden)''<td>1932.gada 2.jūlijs - 1936.gada 2.marts<td> TRP

<tr bgcolor=#FFE8E8>
<td>10.<td>[[Agdanbūgijns Amars]] ''(Agdanbuugiyn Amar)''<td>1936.gada 22.marts - 1939.gada 7.marts<td>TRP

<tr bgcolor=#FFE8E8>
<td>11.<td>[[Horlogijns Čoibalsans]] ''(Horloogiyn Choybalsan)''<td>1939.gada 24.marts - 1952.gada 26.janvāris<td>TRP

<tr bgcolor=#FFE8E8>
<td>12.<td>[[Jumžāgijns Cedenbals]] ''(Yumjaagiyn Tsedenbal)''<td>1952.gada 26.janvāris - 1974.gada 11.jūnijs<td>TRP

<tr bgcolor=#FFE8E8>
<td>13.<td>[[Žanbijns Batmunhs]] ''(Jambyn Batmönh)''<td>1974.gada 11.jūnijs - 1984.gada 12.decembris<td>TRP

<tr bgcolor=#FFE8E8>
<td>14.<td>[[Dumāgijns Sodnoms]] ''(Dumaagiyn Sodnom)''<td>1984.gada 12.decembris - 1990.gada 21.marts<td>TRP

<tr bgcolor=#FFE8E8>
<td>15.<td>[[Šaravijns Gungādoržs]] ''(Sharavyn Gungaadorj)''<td>1990.gada 21.marts - 1990.gada 11.septembris<td>TRP

<tr bgcolor=#FFE8E8>
<td>16.<td>[[Dašijns Bjambasurens]] ''(Dashiyn Byambasüren)''<td>1990.gada 11.septembris - 1992.gada 21.jūlijs<td>TRP

<tr bgcolor=#FFE8E8>
<td>17.<td>[[Puncagijns Žasraijs]] ''(Puntsagiyn Jasray)''<td>1992.gada 21.jūlijs - 1996.gada 19.jūlijs<td>TRP

<tr bgcolor=#DDFFDD>
<td>18.<td>[[Mendsaihanjs Enksaihans]] ''(Mendsayhany Enkhsaikhan)''<td>1996.gada 19.juūlijs - 1998.gada 23.aprīlis<td>DS

<tr bgcolor=#DDEEFF>
<td>19.<td>[[Cakjagijns Elbegdoržs]] ''(Tsakhiagiyn Elbegdorj)''<td>1998.gada 23.aprīlis - 1998.gada 9.decembris<td>NDP

<tr bgcolor=#DDEEFF>
<td>20.<td>[[Žanlavijns Narancacralts]] ''(Janlavyn Narantsatsralt)''<td>1998.gada 9.decembris - 1999.gada 22.jūlijs<td>NDP

<tr bgcolor=#DDEEFF>
<td>21.<td>[[Ņamosorijns Tujā]] ''(Nyam-Osoryn Tuyaa)'' (faktiski)<td>1999.gada 22.jūlijs - 1999.gada 30.jūlijs<td>NDP

<tr bgcolor=#DDEEFF>
<td>22.<td>[[Rinčinjamijns Amaržargals]] ''(Rinchinnyamyn Amarjargal)''<td>1999.gada 30.julijs - 2000.gada 26.jūlijs<td>NDP

<tr bgcolor=#FFE8E8>
<td>23.<td>[[Nambarijns Enkbajars]] ''(Nambaryn Enkhbayar)''<td>2000.gada 26.jūlijs - 2004.gada 20.augusts<td>TRP

<tr bgcolor=#DDEEFF>
<td>24.<td>[[Cakjagijns Elbegdoržs]] ''(Tsakhiagiyn Elbegdorj)''<td>2004.gada 20.augusts - 2006.gada 24.janvāris<td>NDP

<tr bgcolor=#FFE8E8>
<td>23.<td>[[Mijegombins Engbolds]] ''(Миеэгомбын Энхболд)''<td>2006.gada 24.janvāris - <td>TRP

</table>


TRP - [[Mongolijas Tautas Revolucionārā partija]] ''(Mongol Ardyn Khuv'sgatt Nam)'' - komunistiska, pašlaik socialdemokrātiska<br />
DS - [[Mongolijas Demokratiskā Savieniba]]
NDP - [[Mongolijas Nacionāli Demokratiskā partija]]

[[Kategorija:Mongolija]]
"""

begin = time.time()
parsed = textlib.extract_templates_and_params(text, remove_disabled_parts=False, strip=True)
end = time.time()
print(end-begin)