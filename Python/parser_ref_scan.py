#!/usr/bin/python
# -*- coding: utf-8  -*-
"""
pwb.py parser_ref_scan.py lvwiki-20160601-pages-articles.xml

"""
import sys
import re
import csv
import urllib
import pywikibot
from pywikibot import xmlreader
from pywikibot import textlib

f = csv.writer(open("scanLV.txt", "w", encoding='utf-8'))
f.writerow(["Name"])

checklist = ['#expr:', '#if:', '#ifeq:', '#iferror:', '#ifexpr:', '#ifexist:', '#rel2abs:', '#switch', '#time:', '#timel:', '#titleparts:']

for page in xmlreader.XmlDump(sys.argv[1]).parse():
	if page.ns == "10" and not page.isredirect:
		pagetext = textlib.unescape(page.text.lower())
		for parser in checklist:
			count =pagetext.count(parser)
			if count>0:
				outputstring = '{}----{}----{}----{}----'.format('10', page.title, parser, count)
				pywikibot.output(outputstring)
				f.writerow([outputstring])
	if page.ns == "0" and not page.isredirect:
		pagetext = textlib.unescape(page.text.lower())
		count =pagetext.count('</ref>')
		if count>0:
			outputstring = '{}----{}----{}----{}----'.format('0', page.title, '</ref>', count)
			pywikibot.output(outputstring)
			f.writerow([outputstring])